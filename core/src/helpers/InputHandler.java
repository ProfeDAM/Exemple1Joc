package helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Bullet;
import com.mygdx.game.GameScreen;

/**
 * Created by Lluis Maria on 04/05/2018.
 */

public class InputHandler implements InputProcessor {

    private GameScreen screen;

    public InputHandler(GameScreen screen) {
        this.screen = screen;
    }

    @Override


    public boolean keyDown(int keycode) {
        switch (keycode)
        {
            case Input.Keys.UP:
                screen.getSpaceCraft().goUp();
                Gdx.app.log("up", "up");
                break;
            case Input.Keys.DOWN:
                screen.getSpaceCraft().goDown();
                break;
            case Input.Keys.LEFT:
                screen.getSpaceCraft().goLeft();
                break;
            case Input.Keys.RIGHT:
                screen.getSpaceCraft().goRight();
                break;
            case Input.Keys.SPACE:
                Bullet bullet = new Bullet (new Vector2( screen.getSpaceCraft().getPosition().x+screen.getSpaceCraft().getWidth(),
                        screen.getSpaceCraft().getPosition().y +screen.getSpaceCraft().getHeight()/2));
                screen.getStage().addActor(bullet);
                break;

        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
