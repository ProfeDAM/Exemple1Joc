package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import helpers.InputHandler;
import utils.Settings;

/**
 * Created by Lluis Maria on 04/05/2018.
 */

public class GameScreen implements Screen {

    private Stage stage;
    private SpaceCraft spaceCraft;
    private ShapeRenderer shapeRenderer;
    private Batch batch;

    public Stage getStage() {
        return stage;
    }



    public SpaceCraft getSpaceCraft() {
        return spaceCraft;
    }



    public GameScreen() {

        shapeRenderer = new ShapeRenderer();
        OrthographicCamera camera = new OrthographicCamera (Settings.GAME_WIDTH, Settings.GAME_HEIGHT);
        camera.setToOrtho(true);
        StretchViewport viewPort = new StretchViewport(Settings.GAME_WIDTH, Settings.GAME_HEIGHT, camera);

        stage = new Stage(viewPort);
        batch = stage.getBatch();
        spaceCraft = new SpaceCraft(Settings.SPACECRAFT_STARTX, Settings.SPACECRAFT_STARTY, Settings.SPACECRAFT_WIDTH, Settings.SPACECRAFT_HEIGHT, shapeRenderer);

        stage.addActor(spaceCraft);
        Gdx.input.setInputProcessor(new InputHandler(this));
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.draw();
        stage.act(delta);


    }



    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
