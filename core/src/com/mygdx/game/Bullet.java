package com.mygdx.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import utils.AssetManager;
import utils.Settings;

public class Bullet extends Actor {

   private ShapeRenderer shapeRenderer;
   private Vector2 position;


    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public Bullet(Vector2 position) {
        this.shapeRenderer = new ShapeRenderer();
        this.position = position;

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(AssetManager.textureBullet, position.x, position.y, Settings.BULLET_WIDTH, Settings.BULLET_HEIGHT);
        /*

        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(new Color(1, 1, 0, 1));
        shapeRenderer.rect(getPosition().x, getPosition().y, Settings.BULLET_WIDTH,Settings.BULLET_HEIGHT);
        shapeRenderer.end();
        */

    }

    @Override
    public void act(float delta) {
        super.act(delta);
        position.x+= Settings.BULLET_VELOCITY*delta;
    }
}
