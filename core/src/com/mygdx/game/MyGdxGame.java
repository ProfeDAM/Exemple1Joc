package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

import utils.AssetManager;

public class MyGdxGame extends Game {

	
	@Override
	public void create () {

	    Gdx.app.log("LyfeCicle", "create()");
	    AssetManager.load();
	    setScreen(new GameScreen());
    }

    @Override
    public void pause() {
        super.pause();
        Gdx.app.log("LifeCycle", "pause()");
    }

    @Override
    public void resume() {
        super.resume();
        Gdx.app.log("LifeCycle", "resume");

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        Gdx.app.log("LifeCycle", "resize(" + Integer.toString(width) + ", " +
                Integer.toString(height)+")");

    }

    @Override
	public void render () {
         super.render();
        Gdx.app.log("LyfeCicle", "render");
	}
	
	@Override
	public void dispose () {
	    super.dispose();

        Gdx.app.log("LifeCycle", "dispose()");
        AssetManager.dispose();

	}
}
