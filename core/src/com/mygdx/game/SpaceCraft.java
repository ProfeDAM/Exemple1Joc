package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import utils.Settings;

/**
 * Created by Lluis Maria on 04/05/2018.
 */

public class SpaceCraft extends Actor {

    public static final int SPACECRAFT_RIGHT = 0;
    public static final int SPACECRAFT_UP = 1;
    public static final int SPACECRAFT_DOWN = 2;
    public static final int SPACECRAFT_LEFT = 3;

    private Vector2 position;
    private int width, height;
    private int direction;
    private ShapeRenderer shapeRenderer;
    private Texture texture;
    private TextureRegion textureRegion;



    public SpaceCraft(float x, float y, int width, int height, ShapeRenderer shapeRenderer) {
        this.width = width;
        this.height = height;
        position = new Vector2(x, y);
        this.shapeRenderer = shapeRenderer;
        this.texture = new Texture (Gdx.files.internal("jet.jpg"));
        textureRegion = new TextureRegion(texture);
        textureRegion.flip(true, true);



        direction = SPACECRAFT_RIGHT;
    }

    public Vector2 getPosition() {
        return position;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(textureRegion, this.getPosition().x, this.getPosition().y, width, height);



/*
        Gdx.gl20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(new Color(1, 1, 0, 1));
        shapeRenderer.rect(getPosition().x, getPosition().y, getWidth(), getHeight());
    /*    Gdx.app.log("x", String.valueOf(spaceCraft.getX()));
        Gdx.app.log("y", String.valueOf(spaceCraft.getY()));
        Gdx.app.log("width", String.valueOf(spaceCraft.getWidth()));
        Gdx.app.log("heigth", String.valueOf(spaceCraft.getHeight())); */

       // shapeRenderer.rect(100, 20, getWidth(), getHeight()); */


      //  shapeRenderer.end();
        //  Gdx.app.log("drawElements", "draw");


    }

    @Override
    public void act(float delta) {
        super.act(delta);

        switch (direction) {
            case SPACECRAFT_UP:
                if (this.position.y - Settings.SPACECRAFT_VELOCITY * delta >= 0) {
                    this.position.y -= Settings.SPACECRAFT_VELOCITY * delta;
                }
                break;
            case SPACECRAFT_DOWN: {
                if (this.position.y + height + Settings.SPACECRAFT_VELOCITY * delta <=

                        Settings.GAME_HEIGHT) {
                    this.position.y += Settings.SPACECRAFT_VELOCITY * delta;
                }
                break;

            }
            case SPACECRAFT_RIGHT: {
                if (this.position.x + Settings.SPACECRAFT_VELOCITY*delta +
                        this.width <=Settings.GAME_WIDTH)
                {
                    this.position.x += Settings.SPACECRAFT_VELOCITY*delta;
                }
                break;
            }

            case SPACECRAFT_LEFT: {
                if (this.position.x - Settings.SPACECRAFT_VELOCITY*delta >=0)
                {
                    this.position.x -= Settings.SPACECRAFT_VELOCITY*delta;
                }
                break;

            }


        }

    }

    @Override
    public float getWidth() {
        return (float) width;
    }

    @Override
    public float getHeight() {
        return (float) height;
    }

    public int getDirection() {
        return direction;
    }

    public void  goUp()
    {
        direction = SPACECRAFT_UP;
    }
    public void goDown()
    {
        direction = SPACECRAFT_DOWN;
    }
    public void  goLeft()
    {
        direction = SPACECRAFT_LEFT;
    }
    public void  goRight()
    {
        direction = SPACECRAFT_RIGHT;
    }

}
