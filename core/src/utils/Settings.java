package utils;

/**
 * Created by Lluis Maria on 04/05/2018.
 */

public class Settings {
    public static final int GAME_WIDTH = 240;
    public static final int GAME_HEIGHT = 135;

    public static final float SPACECRAFT_VELOCITY = 50;
    public static final int SPACECRAFT_WIDTH = 36;
    public static final int SPACECRAFT_HEIGHT = 18;
    public static final int BULLET_WIDTH = 8;
    public static final int BULLET_HEIGHT = 2;
    public static final int SPACECRAFT_STARTX = 20;
    public static final int SPACECRAFT_STARTY = GAME_HEIGHT/2-SPACECRAFT_HEIGHT/2;
    public static final float BULLET_VELOCITY = SPACECRAFT_VELOCITY*2;



}
